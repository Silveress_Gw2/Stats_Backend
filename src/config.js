const dotenv = require('dotenv')
dotenv.config()

const packageData = require('../package.json')

const dbPort = parseInt(process.env.DB_PORT, 10) || 27017
const dbLocation = process.env.DB_LOCATION
const dbUser = process.env.DB_USER
const dbPass = process.env.DB_PASS

let dbURL = `mongodb://${dbUser}:${dbPass}@${dbLocation}:${dbPort}`;
if (dbUser === '' || dbPass === '') {
  dbURL = `mongodb://${dbLocation}:${dbPort}`;
}

let split = dbLocation.split('.')
if (!isNaN([split.length - 1] - 0)) {
  dbURL = `mongodb+srv://${dbUser}:${dbPass}@${dbLocation}/admin?ssl=false&w=majority`;
}

module.exports = {
  name: packageData.name,
  version: packageData.version,
  port: parseInt(process.env.PORT, 10) || 89,
  env: process.env.ENVIROMENT || 'production',
  db: {
    uri: dbURL,
    database: process.env.DB,
  },
  key: process.env.KEY,
}