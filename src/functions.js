
function beautifyJSON(body,flag) {
  if(flag ==="min" || flag ==="human"){
    if(flag ==="min"){
      return JSON.stringify(body);
    }
    if(flag ==="human"){
      return JSON.stringify(body, null, '\  ');
    }
  }else{
    return JSON.stringify(body, null, '\  ');
  }
}

function querySorter(query,active,endpoint, defaultFields){
  let result = {};
  if(active.beautify) {
    result.beautify = query.beautify || "human";
  }

  result.fields = query.fields || defaultFields || "id,name,lastUpdate,buy_quantity,buy_price,sell_quantity,sell_price";

  result.project = {_id:0};
  if(active.project) {
    let fieldsArray = result.fields.split(",");
    for (let i = 0; i < fieldsArray.length; i++) {
      result.project[fieldsArray[i]] = 1
    }
  }

  result.find = {$and: []};

  if(active.ids){
    let ids = [];
    if(typeof query.ids !== "undefined"){
      ids = query.ids.split(",");
    }
    let idQuery = {$or:[]};
    for(let i=0;i<ids.length;i++){
      idQuery.$or.push({ "id": ids[i] -0})
    }
    if(idQuery.$or.length > 0){
      result.find.$and.push(idQuery);
    }
  }

  if(active.start){
    if (typeof query.start !== "undefined") {
      result.find.$and.push({date: {$gte: new Date(query.start).toISOString()}})
    }
  }
  if(active.end){
    if (typeof query.end !== "undefined") {
      result.find.$and.push({date: {$lte: new Date(query.end).toISOString()}})
    }
  }

  if(active.filter){
    if(result.find.$and.length === 0){
      if(typeof query.filter !== "undefined") {
        result.find = filterSorter(query.filter);
      }
    }else{
      if(typeof query.filter !== "undefined") {
        let filterResult = filterSorter(query.filter);
        let keys = Object.keys(filterResult);
        for (let i = 0; i < keys.length; i++) {
          let tmp = {};
          tmp[keys[i]] = filterResult[keys[i]];
          result.find.$and.push(tmp);
        }
      }
    }
  }

  if(typeof result.find.$and !== "undefined" && result.find.$and.length === 0){
    result.find = {};
    if(endpoint === "items"){
      result.find = {AccountBound: false, marketable: true};
    }
    if(endpoint === "history"){
      result.find = {id:0};
    }
  }
  return result;
}

module.exports = {
  beautifyJSON,
  querySorter
}
