module.exports = function(ctx) {
    require('./routes/precurser.js')(ctx);
    require('./routes/minis.js')(ctx);
    require('./routes/general.js')(ctx);
};