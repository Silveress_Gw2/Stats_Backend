const { beautifyJSON } = require('../functions')
const { key } = require('../config')

module.exports = function (ctx) {
  const db = ctx.database
  const server = ctx.server

  server.post('/gw2_stats/v1/forging/precurser', (req, res, next) => {
    if (req.header('auth') !== key) {
      res.send(401, 'Access denied')
      return next()
    }

    let data = req.body
    data.sell = data.sell - 0

    db.collection('precurser')
		.insertOne(data)
		.then(doc => res.sendRaw(200, beautifyJSON(doc, 'human'), { 'Content-Type': 'application/json; charset=utf-8', 'Access-Control-Allow-Origin': '*', }))
		.catch((err) => {console.error('saving precurser: ' + err);res.send(500, err)})

    next()
  })

  server.get('/gw2_stats/v1/forging/precurser', async (req, res, next) => {
    let beautify = req.query.beautify || 'human'

    let data = await db.collection('precurser')
		.find({})
		.project({ _id: 0 })
		.toArray()

		.catch((err) => {console.error('getting precurser: ' + err); res.send(500, err)})

	  res.sendRaw(200, beautifyJSON(data, beautify), { 'Content-Type': 'application/json; charset=utf-8', 'Access-Control-Allow-Origin': '*', })

    next()
  })
}