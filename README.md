# Silveress's Gw2 API Parser.

This is the source code for my parser that outputs:  
https://api.silveress.ie/gw2/v1/items/json    
https://api.silveress.ie/gw2/v1/items/csv    
https://api.silveress.ie/gw2/v1/recipes/json  
https://api.silveress.ie/gw2/v1/recipes/csv    

## Useage
### Google sheets

``=importdata("https://api.silveress.ie/gw2/v1/items/csv")``  
``=importdata("https://api.silveress.ie/gw2/v1/items/csv?fields=id,name")``  
``=importdata("https://api.silveress.ie/gw2/v1/recipes/csv")``

### Excel

[Microsoft's documentation](https://support.office.com/en-us/article/import-or-export-text-txt-or-csv-files-5250ac4c-663c-47ce-937b-339e391393ba?ui=en-US&rs=en-US&ad=US)  


If you have any suggestions just ask and I will try to incorporate them in.



## Selfhosting it

### Requirements

* Mongodb
* node.js

### Instructions

1. Copy project via git or by extracting the zip [here](https://gitlab.com/Silveress_Gw2/Market_Data_Processer/repository/master/archive.zip)
2. Open cmd or powershell in the folder.
3. Run "npm run firstrun" to get started, this will start mongo, copy the config files and start the api server on port 85.
4. Once complete go to http://localhost:85/gw2/v1/items/json in your browser.
5. Doubleclicking on ``JS/every_1_hour.sh`` with update recipes, ``JS/every_5_min.sh`` will update prices and stats

#### Second Run +
1. To start it after the first time open three (3) cmd windows.
1. In the first window use this cmd "npm run startDBWindows"
2. In the second window use this cmd "npm start"
3. In the third window use this cmd "npm run refreshData" and all the data will be refreshed

### Extras
* The data.json can be edited to fiddle around with stuff
* Silver#5563 on Discord
* Silveress_Golden on reddit  
* [Non API recipes are here](https://docs.google.com/spreadsheets/d/12zHVUJMWiFFIVvDbgwHGx_pdNXG7C-k2731Nb4Gb9W0/edit#gid=0) - Contact me to help.