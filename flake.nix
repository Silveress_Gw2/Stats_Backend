{
  description = "DataWars2 API Stats";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, ... }: utils.lib.eachDefaultSystem (system:
    let

      pkgs = import nixpkgs {
        inherit system;
        overlays = [ ];
      };

      package_name = "api_dw2_stats";
      # name in teh package.json
      package_name_alt = "dw2_stats";

      # set the node version here
      nodejs = pkgs.pkgs.nodejs-16_x;

    in rec {

      packages."${package_name}" = let
          original = pkgs.mkYarnPackage {
            name = "${package_name}";
            buildInputs = [
              nodejs
            ];
            src = ./.;
            packageJSON = "${./package.json}";
            yarnLock = "${./yarn.lock}";
          };
        # takes teh outpout and makes it nicer
        in pkgs.stdenv.mkDerivation {
          name = "${package_name}";
          src = original;
          installPhase = ''
            mkdir -p $out
            cp -R $src/libexec/${package_name_alt}/deps/${package_name_alt}/. $out
            rm $out/node_modules
            cp -R $src/libexec/${package_name_alt}/node_modules/. $out/node_modules
          '';
        };


      defaultPackage = packages."${package_name}";

      nixosModule = { lib, pkgs, config, ... }:
        with lib;
        let
          cfg = config.services."${package_name}";
        in {
          options.services."${package_name}" = {
            enable = mkEnableOption "enable ${package_name}";

            config = mkOption rec {
              type = types.str;
              default = "./.env";
              example = default;
              description = "The env file";
            };

           # specific for teh program running
           prefix = mkOption rec {
              type = types.str;
              default = "silver_";
              example = default;
              description = "The prefix used to name service/folders";
           };

           user = mkOption rec {
              type = types.str;
              default = "${package_name}";
              example = default;
              description = "The user to run the service";
           };

           home = mkOption rec {
              type = types.str;
              default = "/etc/${cfg.prefix}${package_name}";
              example = default;
              description = "The home for the user";
           };

          };

          config = mkIf cfg.enable {

            #users.groups."${cfg.user}" = { };

            #users.users."${cfg.user}" = {
            #  createHome = true;
            #  isSystemUser = true;
            #  home = "${cfg.home}";
            #  group = "${cfg.user}";
            #};

            systemd.services."${cfg.prefix}${cfg.user}" = {
              description = "Dw2 API Stats";

              wantedBy = [ "multi-user.target" ];
              after = [ "network-online.target" ];
              wants = [ ];
              serviceConfig = {
                # fill figure this out in teh future
                DynamicUser=true;
                #User = "${cfg.user}";
                #Group = "${cfg.user}";
                Restart = "always";
                ExecStart = "${nodejs}/bin/node ${self.defaultPackage."${system}"}";
                EnvironmentFile = "${cfg.config}";
              };
            };
          };

        };


    });
}